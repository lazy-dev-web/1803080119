/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

* 文件名  ：led
* 作者    ：yuanyun
* 版本    ：V0.0.1
* 时间    ：2020/10/20
* 描述    ：Led驱动文件
********************************************************************
* 副本
*
*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

/* 头文件 ----------------------------------------------------------------*/
#include"led.h"
/* 宏定义 ----------------------------------------------------------------*/
#define LED1 P1_0
#define LED2 P1_1
#define LED3 P1_4
#define LED_ON 0
#define LED_OFF 1
#define uint unsigned int
#define uchar unsigned char
/* 结构体或枚举 ----------------------------------------------------------------*/
/* 内部函数声明 ----------------------------------------------------------------*/






/* 函数 ----------------------------------------------------------------*/


/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

* 函数名  ：LedInit
* 参数    ：void
* 返回    ：void
* 作者    ：yuanyun
* 时间    ：2020/10/20
* 描述    ：Led初始化
----------------------------------------------------------------*/
void  LedInit(void)
{
    //P1SEL对应位置为通用IO（清零）
    P1SEL &=0xEC;
    //P1DIR对应位设置为输出（置1）
    P1DIR |=0x13;
    //P1对应位设置为灭灯状态（置1）
    P1 |=0x13;
}
/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

* 函数名  ：LedOn
* 参数    ：unsigned char unLedNum 0 所有灯亮；1 D1亮；2 D2亮 ；3 D3亮
* 返回    ：void
* 作者    ：yuanyun
* 时间    ：2020/10/20
* 描述    ：Led开灯
----------------------------------------------------------------*/
void  LedOn(unsigned char unLedNum)
{
    switch(unLedNum)
    {
    case 0:
        LED1 = LED_ON;
        LED2 = LED_ON;
        LED3 = LED_ON;
    case 1:
        LED1 = LED_ON;
        break;
    case 2:
        LED2 = LED_ON;
        break;
    case 3:
        LED3 = LED_ON;
        break;
    default:
        break;
    }
}
/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

* 函数名  ：LedOff
* 参数    ：unsigned char unLedNum 0 所有灯灭；1 D1灭；2 D2灭 ；3 D3灭；
* 返回    ：void
* 作者    ：yuanyun
* 时间    ：2020/10/20
* 描述    ：Led关灯
----------------------------------------------------------------*/
void  LedOff(unsigned char unLedNum)
{
    switch(unLedNum)
    {
    case 0:
        LED1 = LED_OFF;
        LED2 = LED_OFF;
        LED3 = LED_OFF;
    case 1:
        LED1 = LED_OFF;
        break;
    case 2:
        LED2 = LED_OFF;
        break;
    case 3:
        LED3 = LED_OFF;
        break;
    default:
        break;
    }  
}
/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

* 函数名  ：LedToggle
* 参数    ：unsigned char unLedNum 1 D1灭，其他亮；2 D2灭，其他亮；3 D3灭，其他亮
* 返回    ：void
* 作者    ：yuanyun
* 时间    ：2020/10/20
* 描述    ：Led灯翻转
----------------------------------------------------------------*/
void  LedToggle(unsigned char unLedNum)
{
    switch(unLedNum)
    {
    case 1:
        LED1 = LED_OFF;
        LED2 = LED_ON;
        LED3 = LED_ON;
        break;
    case 2:
        LED1 = LED_ON;
        LED2 = LED_OFF;
        LED3 = LED_ON;
        break;
    case 3:
        LED1 = LED_ON;
        LED2 = LED_ON;
        LED3 = LED_OFF;
        break;
    default:
        break;
    } 
}
/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

* 函数名  ：Delay
* 参数    ：uint time
* 返回    ：void
* 作者    ：yuanyun
* 时间    ：2020/10/20
* 描述    ：延时函数
----------------------------------------------------------------*/
void  Delay(uint time)
{
    uint i;
    uchar j;
    for(i=0;i<time;i++)
    {
      for(j=0;j<500;j++)
      {
        asm("NOP");
        asm("NOP"); 
        asm("NOP");
      }
    }
}
/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

* 函数名  ：LED
* 参数    ：void
* 返回    ：void
* 作者    ：yuanyun
* 时间    ：2020/10/20
* 描述    ：灯延时操作
----------------------------------------------------------------*/
void  LED(void)
{
    LED1 = 0;
    LED2 = LED3 =1;
    Delay(1200);
    LED2 = 0;
    LED1 = LED3 =1;
    Delay(1200);
    LED3 = 0;
    LED1 = LED2 =1;
    Delay(1200);
}















